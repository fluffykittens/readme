---
layout: markdown_page
title: "Brie Carranza's README"
---

## Brie Carranza's README

**Brie Carranza, Support Engineer (AMER)** This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to contribute to this page by opening a merge request. :rainbow:

## Related pages

  * [bcarranza.gitlab.io/readme](https://bcarranza.gitlab.io/readme/) - This README served via GitLab Pages
  * [Ansible role testing with multiple Linux distros, using GitLab Container Registry](https://dropnopackets.com/posts/2020-05-ansible-role-testing-molecule-gitlab-container-registry/) over at `dropnopackets.com`
  * [gitlab.com/brie](https://gitlab.com/brie)
  * [my cat Plop](https://about.gitlab.com/company/team-pets/#305-plop-beauregard) - on the GitLab Team Pets page


## About me

[**Consider embedding a video of you working or being interviewed, along with 5-10 bullet points outlining interesting tidbits about you and your interests. Focus these on non-work attributes. Also, add detail on what GitLab values and sub-values resonate most with you. This enables ice-breakers to occur ahead of meetings.**]

## How you can help me

[**Add 5-10 bullet points on what others can do to make your life easier when working with you. Strive to include elements that are nonobvious, or that people would not typically think to ask or consult you about. This enables others to be more efficient in helping you in a way that feels like help.**]

  * Make suggestions for what should go here. 

## My working style

  * This [ticket-talk](https://gitlab.com/bcarranza/ticket-talk) project contains the template I use for **every** ticket and some notes on how I incorporate that template and [Zettlr](https://zettlr.com) into my workflow. 
  * When confronted with a new challenge, I aim to sort things roughly into "what I know about" and "what I don't know about". From there, I aim to find the things in the "what I don't know about" category that interact with the things that I do know, break them into atomic units, learn about them and build them into the "what I know (enough) about" category. 
  * I sometimes forget about things that I know about. 
  * I am more of a generalist than a specialist. I am always interested in doing a deep dive and learning a reasonable amount of specifics on worthwhile topics. 
  * Personality: [Mediator (INFP-T)](https://www.16personalities.com/infp-personality) - This does not dictate everything about my personality but it might prove a useful starting point.   
  * When taking a ticket, my first step is always making sure that there is a clear problem statement. All parties must be working toward solving the same problem. In my experience, working to define the problem clearly increases efficiency (by reducing unnecessary back and forths later in the ticket's lifecycle). 
  * I learn by doing. Test environments where I can break and test things are critical to helping me diagnose and understand a problem. 
  * While I greatly enjoy synchronous brainstorming, I think I do my best troubleshooting and writing solo and asynchronously. I genuinely enjoy watching other people troubleshoot and troubleshooting with them as I am always interested in picking up tips and tricks from my peers. (If you are reading this, please consider scheduling a coffee chat or pairing session. I'd love to get to know you.) If you have learned something the hard way, I would be very happy to benefit from your experience and learn it the easy way from you.  

## What I assume about others

  * I assume positive intent. I aim to act with positive intent. 
  * I assume patience. I aim to be patient. 
  * I assume you will ask me to clarify if I say something that is not clear. I aim to check for understanding and make sure the audience (regardless of size) is onboard. 

[**Add 5-10 bullets on the assumptions you typically hold when working with others. Strive to be as open with these as possible, so others understand your perspective when engaging with you on projects. Remember, the honesty put forth in these answers enables others to be more understanding and empathetic.**]

## What I want to earn

[**Consider 3-5 bullets on your goals for earning things like trust and respect, or a broader understanding of new topics. This enables others to understand what motivates you.**]

## Communicating with me

[**Consider 5-10 bullets on your communication preferences. This includes traditional styles such as verbal, textual, and visual, but you are encouraged to be precise. You can mention things like routine, availability, your travel habits, etc. This helps others understand why you communicate in the manner than you do, and it enables them to tailor their communication in a way that resonates most with you.**]

My preference for communication is via GitLab TODOs, Slack or email, in that order. I am very easygoing so feel free to disturb that order at your leisure. 

When discussig technical components, I will almost always say yes to hearing more detail about what you are explaining to me. 

## Strengths/Weaknesses

[**These may be covered in the above sections. If you prefer a section devoted to strengths and weaknesses, this will enable others to lean on your areas of published expertise and offer support in weak areas without passing judgment.**]

Words that are frequently used to describe me/my work:

  - detailed

### Weaknesses
Iteration [is hard](https://about.gitlab.com/blog/2020/02/04/power-of-iteration/). I am newish to GitLab and am learning to iterate and assemble smaller scopes for work.

Eight months in: iteration ~~is hard~~ requires intention. 

## Tech
  * My first programming language is Python. If I need to do something quickly, I will do it in Python or Go. 
  * I tend towards CLI-based tools and solutions.
  * Text Editors: I default to `vi` unless I am making use of the excellent org-mode in `emacs`.
  * Markdown Editors: [Codium](https://dev.to/0xdonut/why-and-how-you-should-to-migrate-from-visual-studio-code-to-vscodium-j7d) and [Atom](https://atom.io)
  * IDEs: PyCharm, RubyMine, GoLand
  * [Insomnia Core](https://insomnia.rest) - For testing out API calls and then automagically converting a refined `cURL` command to a block of Python/Go/Ruby/whatever. 
  * SSH magic: SSH has so many seemingly simple components that one can build on. Tell me the latest fun thing you did with SSH! (Boring SSH solutions are quite fun!)
  * I have been experimenting with different static site generators. Currently testing replacing Hugo with Gridsome. 

## Characteristics
  * ambitious - I always want to learn and know more. The hard part is deciding which things to focus on and which to defer or decline.
  * enthusiastic - I am a naturally enthusiastic and energetic person. [Burn out](https://www.helpguide.org/articles/stress/burnout-prevention-and-recovery.htm) is real so I manage my enthusiasm to prevent this. 
  * perpetual learner - I enjoy learning new things, whether related to technology or not.
  * planner - I am a planner. I have been described by others as "very organized". My personal organizational approach is the result of developing my organizaitonal skills over time with deliberation and intent. (I am constantly developing my personal workflow and evaluating new tools to replace existing ones. I would love to have a coffee chat to talk about your favorite productivity tools (and/or mine)!)


## Principles

  * The solution should be as simple as possible but no simpler. 
  * Back up the data. Perform meaningful tests of the data restoration process.
  * In difficult situations, sometimes the best thing to do is to set your perspective aside, assume the other party is a rational actor and consider what they might want out of the situation, what their limitations might be, et cetera. Be kind. Remember that there is another human person on the other side of the keyboard (unless it's a dog but you should still be kind to dogs). 
